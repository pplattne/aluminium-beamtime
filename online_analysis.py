import glob
import matplotlib.pyplot as plt
import numpy as np
import os.path
import pandas as pd
import satlas as sat
import scipy.constants

from isotope_info import isotopes
from read_files import read_run, read_calib
from uncertainties import ufloat

# f_27 = 756546472.0443119 # MHz  ## 7.57e8*0.99842735
f_27 = 25235.695*scipy.constants.c/1e6*100 # MHz, don't know what's causing ~640 MHz difference
# print(f_27-756546459.536626)
# CLS is done anti-collinearly

folder = "runs"
isotope = "27"
file_no = 3649
gates = [33, 44] # us
check_gating = False

fit_offset = 0

def hist(vals, bins=None):
    if bins is None:
        x = np.linspace(vals.min(), vals.max(), 25)
    elif isinstance(bins, int):
        x = np.linspace(vals.min(), vals.max(), bins+1)
    else:
        x = bins
    y = np.array([len(vals[vals>=x[i]][vals<x[i+1]]) for i in range(len(x)-1)])

    return (x[:-1]+x[1:])/2, y

def fit(x, y):
    # prepare models
    model = sat.hfsmodel.HFSModel(
        I=isotopes[isotope]["I"],
        J=isotopes[isotope]["J"],
        ABC=isotopes[isotope]["ABC"],
        centroid=isotopes[isotope]["centroid"]-fit_offset,
        scale=y.max()-y.min(),
        background_params=[y.min()],
        shape="voigt",
        sidepeak_params={'Offset': isotopes[isotope]['offset'], 'N':0, 'Poisson': 0.1},
        use_racah=True)

    model.set_variation({'Cl': False, 'Cu': False, 'Offset': False})

    # model.set_variation({"Al": False, "Au": False})
    # model.set_literature_values({"Al": {"value": 94.25, "uncertainty": 0.04}, "Au": {"value": 421, "uncertainty": 15}})

    if np.any([isotope+"m" in key for key in isotopes.keys()]):
        model_m = sat.HFSModel(
            I=isotopes[isotope]["I"],
            J=isotopes[isotope]["J"],
            ABC=isotopes[isotope]["ABC"],
            centroid=isotopes[isotope]["centroid"],
            scale=(y.max()-y.min())*isotopes[isotope+"m"]["scale_m"],
            background_params=[0],
            shape="voigt",
            sidepeak_params={'Offset': isotopes[isotope]['offset'], 'N':0, 'Poisson': 0.1},
            use_racah=True)
        model_m.set_variation({'Cl': False, 'Cu': False, 'Offset': False, 'Background0': False})

        # create sum_model
        model = model+model_m
        model.shared = ['FWHMG', 'FWHML', 'Poisson']

    # print(model.params)
    # for param in model.params:
    #     print(param)
    # exit()

    # do fitting
    sat.fitting.chisquare_fit(model, x, y, yerr=np.sqrt(y)+1, verbose=True)
    model.display_chisquare_fit(show_correl=False)
    # sat.fitting.likelihood_fit(model, x, y, verbose=True)
    # model.display_mle_fit(show_correl=False)

    highRes_x = np.linspace(x.min(), x.max(), 1000)
    highRes_y = model(highRes_x)

    result = model.get_result_frame()
    # print("{:f}".format(result.Centroid.Value[0]))
    plt.plot(x, y, ".", label="data")
    plt.plot(highRes_x, highRes_y, "-", label="best fit")
    plt.xlabel("frequency [MHz]")
    plt.ylabel("photon counts []")
    plt.legend()
    plt.savefig("plots/"+str(file_no)+"_fit.png")
    plt.show()

    # write reference to file and update accordingly
    if isotope == "27":
        if os.path.isfile("references.csv"):
            df = pd.read_csv("references.csv", index_col=0)
        else:
            df = pd.DataFrame(columns=["centroid", "fwhm"])
        df.loc[file_no] = [
            ufloat(result.Centroid.Value[0], result.Centroid.Uncertainty[0]),
            ufloat(result.TotalFWHM.Value[0], result.TotalFWHM.Uncertainty[0])
            ]
        df.to_csv("references.csv")

data = read_run(folder, file_no)
calib = read_calib(folder, file_no)
# clean up data according to Agi's suggestions
data = data[data.channel<=3]
data["scan_voltage"] = data.voltage.map(calib["scan_voltage"])*1000
data["volt_abs"] = data.cooler_voltage.mean()*10000-data.scan_voltage

# check gating
if check_gating:
    plt.hist(data.tof, 100)
    for gate in gates:
        plt.axvline(gate, color="red")
    plt.show()

# keep data within gates
data = data[(data.tof>=gates[0])&(data.tof<=gates[1])]

# calculate frequency
amu_kg = scipy.constants.physical_constants["atomic mass constant"][0]
num = (isotopes[isotope]["mass"]*amu_kg)**2*scipy.constants.c**4
denom = (isotopes[isotope]["mass"]*amu_kg*scipy.constants.c**2+scipy.constants.e*data.volt_abs)**2
beta = np.sqrt(1 - num/denom)
doppler = np.sqrt((1+beta)/(1-beta))

data["freq"] = calib["laser_wavenum"]*2*scipy.constants.c/1e6*100*doppler - f_27
# print(data.freq.mean())

# bin data
x, y = hist(data.freq, 400)

# delete all bins that have 0 counts
x = x[np.where(y!=0)]
y = y[np.where(y!=0)]

# fit and plot
fit(x,y)
