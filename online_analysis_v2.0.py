import glob
import matplotlib.pyplot as plt
import numpy as np
import os.path
import pandas as pd
import satlas as sat
import scipy.constants

from isotope_info import isotopes
from read_files import read_run, read_calib, count
from uncertainties import ufloat

# f_27 = 756546472.0443119 # MHz  ## 7.57e8*0.99842735
f_27 = 25235.695*scipy.constants.c/1e6*100 # MHz, don't know what's causing ~640 MHz difference
# print(f_27-756546459.536626)
# CLS is done anti-collinearly

folder = "runs"
isotope = "27"
save_num = 43
gates = [33, 44] # us
check_gating = False

sum_spectra = {
    "27": [3649, 3653, 3655],
    "26": [3652, 3654, 3656]
}

fit_offset = 0

def hist(vals, bins=None):
    if bins is None:
        x = np.linspace(vals.min(), vals.max(), 25)
    elif isinstance(bins, int):
        x = np.linspace(vals.min(), vals.max(), bins+1)
    else:
        x = bins
    y = np.array([len(vals[vals>=x[i]][vals<x[i+1]]) for i in range(len(x)-1)])

    return (x[:-1]+x[1:])/2, y

def fit(x, y):
    # prepare models
    model = sat.hfsmodel.HFSModel(
        I=isotopes[isotope]["I"],
        J=isotopes[isotope]["J"],
        ABC=isotopes[isotope]["ABC"],
        centroid=isotopes[isotope]["centroid"]-fit_offset,
        scale=y.max()-y.min(),
        background_params=[y.min()],
        shape="voigt",
        sidepeak_params={'Offset': isotopes[isotope]['offset'], 'N':0, 'Poisson': .1},
        use_racah=False)

    model.set_variation({'Cl': False, 'Cu': False, 'Offset': False})
    # model.set_boundaries({"Offset": {"min": -150, "max":0}})

    model.set_expr({"Au": "Al*4.5701"})
    # model.set_variation({"Al": False, "Au": False})
    # model.set_literature_values({"Al": {"value": 94.25, "uncertainty": 0.04}, "Au": {"value": 421, "uncertainty": 15}})

    if np.any([isotope+"m" in key for key in isotopes.keys()]):
        model_m = sat.HFSModel(
            I=isotopes[isotope+"m"]["I"],
            J=isotopes[isotope+"m"]["J"],
            ABC=isotopes[isotope+"m"]["ABC"],
            centroid=isotopes[isotope+"m"]["centroid"],
            scale=(y.max()-y.min())*isotopes[isotope+"m"]["scale_m"],
            background_params=[0],
            shape="voigt",
            sidepeak_params={'Offset': isotopes[isotope+"m"]['offset'], 'N':0, 'Poisson': 0.1},
            use_racah=True)
        model_m.set_variation({'Cl': False, 'Cu': False, 'Offset': False, 'Background0': False})

        if isotopes[isotope]["I"]==0:
            model_m.set_variation({"Al": False, "Au": False, "Bl": False, "Bu": False})

        # create sum_model
        model = model+model_m
        model.shared = ['FWHMG', 'FWHML', 'Poisson']

    # print(model.params)
    # for param in model.params:
    #     print(param)
    # exit()

    # do fitting
    sat.fitting.chisquare_fit(model, x, y, yerr=np.sqrt(y)+1, verbose=True)
    model.display_chisquare_fit(show_correl=False)
    # sat.fitting.likelihood_fit(model, x, y, verbose=True)
    # model.display_mle_fit(show_correl=False)

    highRes_x = np.linspace(x.min(), x.max(), 1000)
    highRes_y = model(highRes_x)

    result = model.get_result_frame()
    # print("{:f}".format(result.Centroid.Value[0]))
    plt.plot(x, y, ".", label="data")
    plt.plot(highRes_x, highRes_y, "-", label="best fit")
    plt.xlabel("frequency [MHz]")
    plt.ylabel("photon counts []")
    plt.legend()
    plt.savefig("plots/"+isotope+"Al_"+str(save_num)+".png")
    plt.show()

    # write reference to file and update accordingly
    if isotope == "27":
        if os.path.isfile("references.csv"):
            df = pd.read_csv("references.csv", index_col=0)
        else:
            df = pd.DataFrame(columns=result.columns.levels[0])
        summary = []
        for para in result.columns.levels[0]:
            if para in ["Chisquare", "Reduced chisquare", "NDoF"]:
                summary = np.append(summary, result[para][0])
            else:
                summary = np.append(summary, ufloat(result[para].Value[0], result[para].Uncertainty[0]))
        df.loc[save_num] = summary
        print(df)
        df.to_csv("references.csv")

df = pd.DataFrame()
cooler_mean = 0
calib_mean = None
for file_no in sum_spectra[isotope]:

    data = read_run(folder, file_no)
    calib = read_calib(folder, file_no)
    # clean up data according to Agi's suggestions
    data = data[data.channel<=3]
    data["scan_voltage"] = data.voltage.map(calib["scan_voltage"])*1000
    data["volt_abs"] = data.cooler_voltage.mean()*10000-data.voltage

    cooler_mean += data.cooler_voltage.mean()*10000/len(sum_spectra[isotope])
    if calib_mean is None: 
        calib_mean = calib["scan_voltage"]*1000/len(sum_spectra[isotope])
    else:
        calib_mean += calib["scan_voltage"]*1000/len(sum_spectra[isotope])

    df = df.append(data, ignore_index=True)

data = df

# check gating
if check_gating:
    plt.hist(data.tof, 100)
    for gate in gates:
        plt.axvline(gate, color="red")
    plt.show()

# keep data within gates
data = data[(data.tof>=gates[0])&(data.tof<=gates[1])]


# print(data.freq.mean())

# bin data
x, y = count(data.voltage)
x = calib_mean[x]

x = cooler_mean - x

# calculate frequency
amu_kg = scipy.constants.physical_constants["atomic mass constant"][0]
num = (isotopes[isotope]["mass"]*amu_kg)**2*scipy.constants.c**4
denom = (isotopes[isotope]["mass"]*amu_kg*scipy.constants.c**2+scipy.constants.e*x)**2
beta = np.sqrt(1 - num/denom)
doppler = np.sqrt((1+beta)/(1-beta))

x = calib["laser_wavenum"]*2*scipy.constants.c/1e6*100*doppler - f_27

# fit and plot
fit(x,y)
