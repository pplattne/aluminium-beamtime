import glob
import numpy as np
import pandas as pd

def read_run(path, run_num):
    """
    Parameters
    ----------
    path : String
        File path to file that needs to be read in.
    run_num : int
        Run number to read data.

    Returns
    -------
    pd.DataFrame 
        DataFrame containing events.

    Raises
    ------
    FileNotFoundError
        If file does not exist.
    """
    return pd.read_csv(path+"/run_"+str(run_num)+".csv", names=["time", "voltage", "bunch_num", "channel", "tof", "cooler_voltage"], index_col=False)

def read_calib(path, run_num):
    """
    Parameters
    ----------
    path : String
        File path to file that needs to be read in.
    run_num : int
        Run number to read calibration file.

    Returns
    -------
    dict
        Dict containing calibration data.
        "cooler" - mean of cooler voltage
        "laser_wavenum" - fundamental wavenumber
        "scan_voltage" - pd.Series : scan voltage calibration data

    Raises
    ------
    FileNotFoundError
        If file does not exist.
    """
    result = {}
    with open(path+"/calib_"+str(run_num)+".csv") as f:
        result["cooler"] = float(f.readline().strip("\n").split(": ")[1])
        result["laser_wavenum"] = float(f.readline().strip("\n").split(": ")[1])
    result["scan_voltage"] = pd.read_csv(path+"/calib_"+str(run_num)+".csv", skiprows=[0,1], names=["voltage"]).voltage
    return result

def count(vals):
    x = np.unique(vals)
    y = np.array([len(vals[vals==t]) for t in x])
    
    return x,y

if __name__ == "__main__":
    print(read_calib("sample_runs/", 3610))