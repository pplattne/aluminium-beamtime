import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from uncertainties import ufloat, unumpy

df = pd.read_csv("references.csv", index_col=0)
for col in df.columns:
    if col not in ["Chisquare", "Reduced chisquare", "NDoF"]:
        df[col] = df[col].apply(lambda x: ufloat(x))
plot = ["Al", "Au", "Centroid", "Bl"]

fig, axes = plt.subplots(int(np.ceil(np.sqrt(len(plot)))), int(np.ceil(np.sqrt(len(plot)))))

x_labels = {
    40: "racah fixed",
    41: "racah free",
    42: "r. free, Au==Al*4.57",
    43: "same, with cal. mean",
    3649: "3649",
    3653: "3653",
    3655: "3655"
}

lit = {
    "Al": 94.25,
    "Au": 431.11,
    "Bl": 18.8
}

for ax, val in zip(axes.flatten(), plot):
    ax.errorbar(df.index.map(x_labels), unumpy.nominal_values(df[val]), fmt=".", yerr=unumpy.std_devs(df[val]), label="centroid")
    # ax.set_xlabel("arb. file no. []")
    ax.set_ylabel("value [units]")
    ax.set_title(val)

    if val in lit.keys():
        ax.axhline(lit[val], color="black")
# axes[1].errorbar(x_labels, unumpy.nominal_values(df.fwhm), fmt=".", color="tab:orange", yerr=unumpy.std_devs(df.fwhm), label="FWHM")
# axes[1].set_xlabel("enumeration []")
# axes[1].set_ylabel("FWHM_total [MHz]")

# plt.legend()
plt.show()